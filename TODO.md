# To do:

- [ ] pkginstall: Run as non root. Use doas for commands that need root.
- [ ] pkgremove: Run as non root. Use doas for commands that need root.
- [ ] pre-install: Run as non root. Use doas for commands that need root.
- [ ] pre-remove: Run as non root. Use doas for commands that need root.
- [ ] post-install: Run as non root. Use doas for commands that need root.

root commands should have user consent. User can add those commands
to doas.conf if they don't want to enter password. This increase security.
