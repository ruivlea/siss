# siss
My personal repository of source based linux distribution
that use **S**uckless and K**ISS** philosophy.

I tried to build a system that use:
- sinit
- sbase
- ubase
- musl
- mksh
- opendoas
- openntpd
- libressl

All packages in core must be static.

Suckless or Suckless recommended programs are prioritized to be used.

I started to build from kisslinux.
1. Use official release of kisslinux
2. Then i use repo from kisslinux community and kiss-xorg
3. Create my own pkgtools
4. Switch from kiss packaging tools with my pkgtools
5. Repo from kiss-community, kiss-xorg, carbslinux, alpinelinux,
   and few from archlinux used as reference for build scripts.
   Main references are kiss-community and kiss-xorg,
   copy paste and modified as my needs.

Not yet implemented:
- pkgtools: conflict handling

## Notes:
- To make efistub works, use efi (vfat) partition and put kernel there.

  e.g. /dev/sda1 is efi partition and /dev/sda3 is root:

  ```
  # mount -t efivarfs efivarfs /sys/firmware/efi/efivars
  # efibootmgr -c -d /dev/sda -p 1 -l /linux -L "siss" \
               -u "root=/dev/sda3 loglevel=3 quiet ro"
  ```

- xf86-video-intel needed to fix intel screen tearing.
- xf86-input-wacom needed to enable wacom pressure.

## Other systems that use suckless programs:
- oasis: https://github.com/oasislinux/oasis
- morpheus: https://git.2f30.org/morpheus/
- A_OS: https://git.sdf.org/midfavila/A_OS/
- kiss-dumpsterfire: https://github.com/hovercats/kiss-dumpsterfire/

## References (Thanks to):
- https://codeberg.org/kiss-community/repo
- https://codeberg.org/kiss-community/community
- https://github.com/ehawkvu/kiss-xorg
- https://github.com/hovercats/kiss-dumpsterfire/
- https://git.carbslinux.org/repository
- https://git.alpinelinux.org/aports
- https://github.com/oasislinux/oasis
- Linux From Scratch
- Gentoo
- Archlinux
