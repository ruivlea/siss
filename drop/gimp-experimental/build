#!/bin/sh -e

export DESTDIR=$1

# no test, no bash
sed "/subdir('tests')/d" app/meson.build > _
mv -f _ app/meson.build

# Below will allow the build to continue without gettext.
sed \
    -e "/subdir('po/d" \
    -e "/subdir('desktop')/d" \
    meson.build > _
mv -f _ meson.build
sed \
    -e "/subdir('tags')/d" \
    -e "/subdir('tips')/d" \
    data/meson.build > _
mv -f _ data/meson.build
sed "/subdir('goat/d" extensions/meson.build > _
mv -f _ extensions/meson.build

# Change icons to Color, since the Symbolic icons cause build failures.
sed "/DEFAULT_ICON_THEME/s/Symbolic/Color/" app/config/gimpguiconfig.h > _
mv -f _ app/config/gimpguiconfig.h

# If this sed isn't ran, the vector icons will still be built.
sed "/have_vector_icons/s/not//" icons/Symbolic/meson.build > _
mv -f _ icons/Symbolic/meson.build

# Correct the version of mypaint-brushes.
sed "s/mypaint-brushes-1.0/mypaint-brushes-2.0/" meson.build > _
mv -f _ meson.build

# Change 'babl' to 'babl-0.1' since the name of the pc file changed.
sed "/dependency('babl'/s/'babl'/'babl-0.1'/" meson.build > _
mv -f _ meson.build

meson setup \
    -Dprefix=/usr \
    -Db_colorout=never \
    -Dcheck-update=no \
    -Dmng=disabled \
    -Dwmf=disabled \
    -Dopenexr=disabled \
    -Dheadless-tests=disabled \
    -Dappdata-test=disabled \
    -Dgi-docgen=disabled \
    -Dg-ir-doc=false \
    -Dvector-icons=false \
    -Djavascript=false \
    -Dpython=false \
    -Dlua=false \
    -Dvala-plugins=disabled \
    output

ninja -C output
ninja -C output install

for size in 16x16 32x32 48x48 64x64 256x256 scalable; do
    mkdir -p "$1/usr/share/icons/hicolor/$size/apps/"
    cp desktop/$size/gimp.* "$1/usr/share/icons/hicolor/$size/apps/"
done

rm -rf "$1/usr/share/gtk-doc"
find "$1" -name '*.la' -type f -exec rm -rf {} +

for f in $1/lib/pkgconfig/*; do
	sed 's:prefix=/:prefix=:' $f > _
	mv -f _ $f
done
