#!/bin/sh -e

# PKG_GPU from /etc/profile
[ "$PKG_GPU" = "amd" ] && targets="host;AMDGPU" || targets=host
triple=`cc -dumpmachine`
[ -f /usr/bin/clang ] && CC=clang
[ -f /usr/bin/clang++ ] && CXX=clang++
[ -f /usr/bin/ld.lld ] && lld=ON
[ -f /usr/lib/$triple/libc++.so ] && libcxx=ON && cxx_stdlib=libc++
[ -f /usr/lib/$triple/libunwind.so ] && unwindlib=libunwind
[ -f /usr/lib/clang/$2/lib/x86_64-pc-linux-musl/libclang_rt.builtins.a ] && \
    rtlib=compiler-rt
# From /etc/profile or here. toolchain or framework.
#PKG_LLVM=toolchain
if [ "$PKG_LLVM" = "toolchain" ]; then
#-DLLVM_STATIC_LINK_CXX_STDLIB=ON
#-DLLVM_OPTIMIZED_TABLEGEN=ON
#-DCLANG_LINK_CLANG_DYLIB=OFF
#-DCLANG_ENABLE_STATIC_ANALYZER=OFF
#-DCLANG_ENABLE_ARCMT=OFF
#-DCLANG_TOOLING_BUILD_AST_INTROSPECTION=OFF
#-DLIBCLANG_BUILD_STATIC=ON
#-DCOMPILER_RT_BUILD_XRAY=OFF
#-DCOMPILER_RT_BUILD_MEMPROF=OFF
#-DCOMPILER_RT_BUILD_ORC=OFF
#-DCOMPILER_RT_BUILD_LIBFUZZER=OFF
#-DCOMPILER_RT_BUILD_PROFILE=OFF
#-DLIBCXX_HAS_GCC_S_LIB=OFF
#-DLIBCXX_ENABLE_STATIC_ABI_LIBRARY=ON
#-DLIBCXX_STATICALLY_LINK_ABI_IN_SHARED_LIBRARY=ON
#-DLIBCXX_STATICALLY_LINK_ABI_IN_STATIC_LIBRARY=ON
#-DLIBCXXABI_ENABLE_ASSERTIONS=ON
#-DLIBCXXABI_ENABLE_STATIC_UNWINDER=ON
#-DLIBCXXABI_STATICALLY_LINK_UNWINDER_IN_SHARED_LIBRARY=YES
#-DLIBUNWIND_INSTALL_HEADERS=ON
#-DLIBUNWIND_ENABLE_STATIC=ON
    toolchain_opts="-DLLVM_ENABLE_RUNTIMES='compiler-rt;libunwind;libcxxabi;libcxx'
-DLLVM_ENABLE_LIBCXX=${libcxx:-OFF}
-DLLVM_ENABLE_LLD=${lld:-OFF}
-DCLANG_BUILD_EXAMPLES=OFF
-DCLANG_INCLUDE_DOCS=OFF
-DCLANG_INCLUDE_TESTS=OFF
-DCLANG_DEFAULT_CXX_STDLIB=$cxx_stdlib
-DCLANG_DEFAULT_RTLIB=$rtlib
-DCLANG_DEFAULT_UNWINDLIB=$unwindlib
-DCOMPILER_RT_INCLUDE_TESTS=OFF
-DCOMPILER_RT_BUILD_GWP_ASAN=OFF
-DCOMPILER_RT_BUILD_SANITIZERS=OFF
-DLIBCXX_HAS_MUSL_LIBC=ON
-DLIBCXX_HAS_ATOMIC_LIB=OFF
-DLIBCXX_INSTALL_LIBRARY=ON
-DLIBCXX_USE_COMPILER_RT=ON
-DLIBCXXABI_INSTALL_LIBRARY=ON
-DLIBCXXABI_USE_COMPILER_RT=ON
-DLIBCXXABI_USE_LLVM_UNWINDER=ON
-DLIBCXXABI_ENABLE_STATIC=ON
-DLIBUNWIND_USE_COMPILER_RT=ON"
    projects="llvm;clang;lld"
else
    projects="llvm"
fi

symlink() {
    ln -s llvm-ar "$1/usr/bin/ar"
    ln -s llvm-ranlib "$1/usr/bin/ranlib"
    ln -s llvm-objcopy "$1/usr/bin/objcopy"
    ln -s llvm-objdump "$1/usr/bin/objdump"
    ln -s llvm-readelf "$1/usr/bin/readelf"
    ln -s llvm-strip "$1/usr/bin/strip"
    ln -s llvm-nm "$1/usr/bin/nm"
    ln -s clang "$1/usr/bin/cc"
    ln -s clang "$1/usr/bin/c89"
    ln -s clang "$1/usr/bin/c99"
    ln -s clang++ "$1/usr/bin/c++"
    ln -s ld.lld "$1/usr/bin/ld"
}

pkgconf_file() {
    # Use new llvm-config to generate a pkg-config file.
    PATH="$1/usr/bin:$PATH"
    mkdir -p "$1/usr/lib/pkgconfig"

    sed -e "s|$1||" -e 's|/usr/include|\${includedir}|' \
        > "$1/usr/lib/pkgconfig/llvm.pc" <<EOF
prefix=/usr
exec_prefix=\${prefix}
libdir=\${exec_prefix}/lib
includedir=\${prefix}/include
has_rtti=$(llvm-config --has-rtti)

Name: LLVM
Description: LLVM compiler infrastructure
URL: https://llvm.org
Version: $(llvm-config --version)
Requires:
Requires.private: zlib
Libs: -L\${libdir} $(llvm-config --libs)
Libs.private: -lstdc++ $(llvm-config --libs --link-static)
Cflags: $(llvm-config --cflags)
EOF
}

    #-DLLVM_TOOL_LLVM_ITANIUM_DEMANGLE_FUZZER_BUILD=OFF \
    #-DLLVM_TOOL_LLVM_MC_ASSEMBLE_FUZZER_BUILD=OFF \
    #-DLLVM_TOOL_LLVM_MC_DISASSEMBLE_FUZZER_BUILD=OFF \
    #-DLLVM_TOOL_LLVM_OPT_FUZZER_BUILD=OFF \
    #-DLLVM_TOOL_LLVM_MICROSOFT_DEMANGLE_FUZZER_BUILD=OFF \
    #-DLLVM_TOOL_LLVM_GO_BUILD=OFF \
    #-DLLVM_APPEND_VC_REV=OFF \
    #-DLLVM_INSTALL_UTILS=ON \
    #-DLLVM_ENABLE_BACKTRACES=OFF \
    #-DLLVM_INCLUDE_GO_TESTS=OFF \
    #-DLLVM_INCLUDE_BENCHMARKS=OFF \
cmake -B build \
    -DCMAKE_C_COMPILER=${CC:-gcc} \
    -DCMAKE_CXX_COMPILER=${CXX:-g++} \
    -DCMAKE_C_FLAGS="$CFLAGS -fPIC" \
    -DCMAKE_CXX_FLAGS="$CXXFLAGS -fPIC" \
    -DCMAKE_COLOR_DIAGNOSTICS=OFF \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DLLVM_TARGETS_TO_BUILD="$targets" \
    -DLLVM_HOST_TRIPLE=$triple \
    -DLLVM_ENABLE_PROJECTS="$projects" \
    -DLLVM_ENABLE_PIC=ON \
    -DLLVM_ENABLE_LIBXML2=OFF \
    -DLLVM_ENABLE_ZSTD=OFF \
    -DLLVM_ENABLE_LTO=OFF \
    -DLLVM_ENABLE_ZLIB=OFF\
    -DLLVM_ENABLE_LIBEDIT=OFF \
    -DLLVM_ENABLE_EH=ON \
    -DLLVM_ENABLE_RTTI=ON \
    -DLLVM_ENABLE_TERMINFO=OFF \
    -DLLVM_BUILD_LLVM_DYLIB=ON \
    -DLLVM_LINK_LLVM_DYLIB=ON \
    -DLLVM_INCLUDE_BENCHMARKS=OFF \
    -DLLVM_INCLUDE_EXAMPLES=OFF \
    -DLLVM_INCLUDE_DOCS=OFF \
    -DLLVM_INCLUDE_TESTS=OFF \
    $toolchain_opts \
    -Wno-dev \
    llvm

cmake --build build
DESTDIR=$1 cmake --install build

[ "$PKG_LLVM" = "toolchain" ] && symlink "$1" || pkgconf_file "$1"
