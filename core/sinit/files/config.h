static char *const rcinitcmd[] = { "/etc/boot", NULL };
static char *const rcrebootcmd[] = { "/usr/bin/shutdown", "-r", NULL };
static char *const rcpoweroffcmd[] = { "/usr/bin/shutdown", "-p", NULL };
