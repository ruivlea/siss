#!/bin/sh -e

pkgname=gcc
url='https://gcc.gnu.org/pub/gcc/releases/?C=M;O=D'
pkgdir=$(readlink -f $(dirname "$0"))
verme=$(cut -d ' ' -f 1 "$pkgdir/version")

mkdir -p "/pkg/tmp/$pkgname/src"
cd "/pkg/tmp/$pkgname/src"

verup=$(curl -sS "$url" | grep 'gcc-' | sed -n 1p | cut -d 'gcc-' -f 2 \
        | cut -d '/' -f 1)

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

url="${url%/?*}/gcc-$verup"
src="
$url/gcc-$verup.tar.xz
$url/gcc-$verup.tar.xz.sig
$url/sha512.sum
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	xz -dc "gcc-$verup.tar.xz" | pax -r

	echo "Scanning ..."
	ls | grep -v "gcc-$verup.tar.xz$" | clamdscan --fdpass --no-summary -i

	rm -rf "gcc-$verup"

	for f in "gcc-$verup.tar.xz.sig" "gcc-$verup.tar.xz"; do
		sumup=$(grep "$f$" sha512.sum | cut -d ' ' -f 1)
		sum=$(sha512sum "$f" | cut -d ' ' -f 1)
		if [ "$sum" != "$sumup" ]; then
			echo "$pkgname: $f sum failed"
			exit 1
		fi
	done

	gpg --verify "gcc-$verup.tar.xz.sig" "gcc-$verup.tar.xz"

	echo "Updating $pkgname repo files"
	sum=$(b3sum "gcc-$verup.tar.xz")

	sed "s|.*gcc.*.tar.xz|$sum|" "$pkgdir/checksums" > _
	mv -f _ "$pkgdir/checksums"

	sed "s/$verme/$verup/g" "$pkgdir/sources" > _
	mv -f _ "$pkgdir/sources"

	echo "$verup 1" > "$pkgdir/version"
	echo "$pkgname: $verme -> $verup"
fi
