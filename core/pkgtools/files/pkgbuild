#!/bin/sh -e

build()
{
	rm -rf "/pkg/tmp/$pkgname/out"
	rm -f "/pkg/tmp/$pkgname/build.log"
	cd "/pkg/tmp/$pkgname/work"
	if ! \
		PATH=/usr/bin \
		AR=ar \
		CC=cc \
		CXX='c++' \
		NM=nm \
		RANLIB=ranlib \
		RUSTFLAGS="--remap-path-prefix=$PWD=. $RUSTFLAGS" \
		GOFLAGS="-trimpath -modcacherw $GOFLAGS" \
		GOPATH="$PWD/go" \
		"$pkgpath/build" "/pkg/tmp/$pkgname/out" "$pkgver" 2>&1; then
		kill 0
	fi | tee "/pkg/tmp/$pkgname/build.log"
}

if [ "$(id -u)" = "0" ]; then
	echo "Avoid build with root privilege"
	exit 1
fi

if [ -z "$1" ]; then
	echo "usage: pkgbuild pkgname"
	exit 1
fi

case "$1" in
*[![:alnum:]+_\-]*)
	echo 'Only alphanumeric and "+ - _" are allowed in pkgname'
	exit 1
	;;
esac

pkgname="$1"
echo "Will build $pkgname"
pkgpath=$(pkgfind "$pkgname" | sed '/installed/d' | head -n 1)
if [ "$pkgpath" = "$pkgname not found" ]; then
	echo "$pkgpath"
	exit 1
fi
pkgver=$(cut -d ' ' -f 1 "$pkgpath/version")
pkgrel=$(cut -d ' ' -f 2 "$pkgpath/version")

# Download src
[ -f "$pkgpath/sources" ] && [ ! -d "/pkg/tmp/$pkgname/src" ] \
    && pkgdownloadsrc "$pkgname"

# Check depends
if [ -f "$pkgpath/depends" ]; then
	depends=$(cut -d ' ' -f 1 "$pkgpath/depends" | sed '/^#/d;/^$/d')
	needdep=
	for dep in $depends; do
		if [ ! -d "/pkg/installed/$dep" ]; then
			needdep="$needdep\n$dep\n"
		fi
	done
	
	if [ ! -z "$needdep" ]; then
		echo "Please install this dependency first:"
		echo "$needdep"
		exit 1
	fi
fi

# Extract
if [ -f "$pkgpath/sources" ]; then
	pkgextractsrc "$pkgname"
else
	rm -rf "/pkg/tmp/$pkgname/work"
	mkdir -p "/pkg/tmp/$pkgname/work" 
fi

# Build
if [ ! -f "$pkgpath/build" ]; then
	echo "$pkgpath/build not found"
	exit 1
fi

if [ ! -x "$pkgpath/build" ]; then
	echo "$pkgpath/build not executable"
	exit 1
fi

echo "Build time exclude packaging:" > "/pkg/tmp/$pkgname/buildtime"
(time build) 2>> "/pkg/tmp/$pkgname/buildtime" && pkgpack "$pkgname" \
    && cat "/pkg/tmp/$pkgname/buildtime"
