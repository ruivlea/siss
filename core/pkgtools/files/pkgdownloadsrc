#!/bin/sh -e

pkgdownloadsrc()
{
	case "$1" in
	*[![:alnum:]+_\-]*)
		echo 'Only alphanumeric and "+ - _" are allowed in pkgname'
		exit 1
		;;
	esac

	pkgname="$1"
	pkgpath=$(pkgfind "$pkgname" | sed '/installed/d' | head -n 1)
	if [ "$pkgpath" = "$pkgname not found" ]; then
		echo "$pkgpath"
		exit 1
	fi
	pkgver=$(cut -d ' ' -f 1 "$pkgpath/version")
	pkgrel=$(cut -d ' ' -f 2 "$pkgpath/version")
	srcs=$(cat "$pkgpath/sources")
	echo "$srcs"
	mkdir -p "/pkg/tmp/$pkgname/src"
	srcsdl=$(echo "$srcs" | cut -d ' ' -f 1)
	line=1
	for src in $srcsdl; do
		srcdest=$(echo "$srcs" | sed "${line}q;d" | cut -d ' ' -f 2 -s)
		[ ! -z "$srcdest" ] && mkdir -p "/pkg/tmp/$pkgname/src/$srcdest"
		cd "/pkg/tmp/$pkgname/src/$srcdest"

		case "$src" in
		git*)
			branch=""
			url=$(echo "$src" | cut -d '+' -f 2 | cut -d '@' -f 1 \
                  | cut -d '#' -f 1)

			if echo "$src" | grep -q '@'; then
				branch=$(echo "$src" | cut -d '@' -f 2 | cut -d '#' -f 1)
			fi

			if [ ! -z "$branch" ]; then
				echo "git clone --depth 1 -b $branch $url"
				git clone --depth 1 -b "$branch" "$url"
			else
				echo "git clone $url"
				git clone "$url"
			fi

			if echo "$src" | grep -q '#'; then
				commit=$(echo "$src" | cut -d '#' -f 2)
				dir=$(basename "$url" | cut -d '.git' -f 1)
				cd "$dir"
				echo "git reset --hard $commit"
				git reset --hard "$commit"
			fi
			;;
		*?no-extract)
			url=$(echo "$src" | cut -d '?no-extract' -f 1)
			file="$(basename $url)?no-extract"
			[ ! -f "$file" ] && curl -fL "$url" -o "$file"
			;;
		http* | *::http*)
			url=$(echo "$src" | cut -d '::' -f 2)
			file=$(basename "$url")
			[ ! -f "$file" ] && curl -fLO "$url"
			;;
		esac

		line=$((line + 1))
	done
}

if [ -z "$1" ]; then
	echo "usage: pkgdownloadsrc pkgname"
	exit 1
fi

case "$@" in
*[![:alnum:]+_\ -]*)
	echo 'Only alphanumeric and "+ - _" are allowed'
	exit 1
	;;
esac

set +e
pkgs=$(pkgsort "$@")
set -e

case "$pkgs" in *' not found') echo "$pkgs" && exit 1 ;; esac

for pkg in $pkgs; do
	pkgdownloadsrc "$pkg"
done
