#!/bin/sh -e

pkgname=pigz
url='https://zlib.net/pigz/'
pkgdir=$(readlink -f $(dirname "$0"))
verme=$(cut -d ' ' -f 1 "$pkgdir/version")

mkdir -p "/pkg/tmp/$pkgname/src"
cd "/pkg/tmp/$pkgname/src"

echo "Downloading $url for version checking"
curl -f "$url" -o _

verup=$(grep 'version' _ | head -n 1 | cut -d '-' -f 2 | cut -d '.t' -f 1)
sumup=$(grep 'SHA' _ | head -n 1 | cut -d 'm ' -f 2 | cut -d ',' -f 1)

rm -f _

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

src="
${url}pigz-$verup.tar.gz
${url}pigz-$verup-sig.txt
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	xz -dc "pigz-$verup.tar.gz" | pax -r

	echo "Scanning ..."
	ls | grep -v "pigz-$verup.tar.gz$" | clamdscan --fdpass --no-summary -i

	rm -rf "pigz-$verup"

	sum=$(sha256sum "pigz-$verup.tar.gz" | cut -d ' ' -f 1)
	if [ "$sum" != "$sumup" ]; then
		echo "$pkgname: pigz-$verup.tar.gz sum failed"
		exit 1
	fi

	gpg --verify "pigz-$verup-sig.txt" "pigz-$verup.tar.gz"

	echo "Updating $pkgname repo files"
	sum=$(b3sum "pigz-$verup.tar.gz")

	sed "s|.*pigz.*.tar.gz|$sum|" "$pkgdir/checksums" > _
	mv -f _ "$pkgdir/checksums"

	sed "s/$verme/$verup/" "$pkgdir/sources" > _
	mv -f _ "$pkgdir/sources"

	echo "$verup 1" > "$pkgdir/version"
	echo "$pkgname: $verme -> $verup"
fi
