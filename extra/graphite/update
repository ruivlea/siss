#!/bin/sh -e

pkgname=graphite
url=https://github.com/silnrsi/graphite/releases
pkgdir=$(readlink -f $(dirname $0))
verme=$(cut -d ' ' -f 1 $pkgdir/version)

mkdir -p /pkg/tmp/$pkgname/src
cd /pkg/tmp/$pkgname/src

verup=$(curl -sS "$url" | grep 'tag/' | sed -n 1p | cut -d 'g/' -f 2 \
        | cut -d '"' -f 1)

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

url=$url/download/$verup
src="
$url/graphite2-$verup.tgz
$url/graphite2-$verup.sha256sum
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	gzip -dc graphite2-$verup.tgz | pax -r

	echo "Scanning ..."
	ls | grep -v "graphite2-$verup.tgz$" | clamdscan --fdpass --no-summary -i

	rm -rf graphite2-$verup

	echo "Checking sum ..."
	sumup=$(grep "graphite2-$verup.tgz" graphite2-$verup.sha256sum \
            | cut -d ' ' -f 1)
	sum=$(sha256sum graphite2-$verup.tgz | cut -d ' ' -f 1)
	if [ "$sum" != "$sumup" ]; then
		echo "$pkgname: graphite2-$verup.tgz sum failed"
		exit 1
	fi

	echo "Updating $pkgname repo files"
	sum=$(b3sum graphite2-$verup.tgz)

	sed "s|.*graphite2.*.tgz|$sum|" $pkgdir/checksums > _
	mv -f _ $pkgdir/checksums

	sed "s/$verme/$verup/g" $pkgdir/sources > _
	mv -f _ $pkgdir/sources

	echo "$verup 1" > $pkgdir/version
	echo "$pkgname: $verme -> $verup"
fi
