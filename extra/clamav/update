#!/bin/sh -e

pkgname=clamav
url='https://github.com/Cisco-Talos/clamav'
pkgdir=$(readlink -f $(dirname "$0"))
verme=$(cut -d ' ' -f 1 "$pkgdir/version")

mkdir -p "/pkg/tmp/$pkgname/src"
cd "/pkg/tmp/$pkgname/src"

verup=$(curl -sS "$url" | grep 'tag/' | cut -d 'v-' -f 2 | cut -d '"' -f 1 \
        | grep -v 'rc' | head -n 1)

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

url='https://www.clamav.net/downloads/production'
src="
$url/clamav-$verup.tar.gz
$url/clamav-$verup.tar.gz.sig
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	gzip -dc "clamav-$verup.tar.gz" | pax -r

	echo "Scanning ..."
	ls | grep -v "clamav-$verup.tar.gz$" | clamdscan --fdpass --no-summary -i

	rm -rf "clamav-$verup"

	gpg --verify "clamav-$verup.tar.gz.sig" "clamav-$verup.tar.gz"

	echo "Updating $pkgname repo files"
	sum=$(b3sum "clamav-$verup.tar.gz")

	sed "s|.*clamav.*.tar.gz|$sum|" "$pkgdir/checksums" > _
	mv -f _ "$pkgdir/checksums"

	sed "s/$verme/$verup/" "$pkgdir/sources" > _
	mv -f _ "$pkgdir/sources"

	echo "$verup 1" > "$pkgdir/version"
	echo "$pkgname: $verme -> $verup"
fi
