#!/bin/sh -e

pkgname=adwaita-icon-theme
url='https://gitlab.gnome.org/GNOME/adwaita-icon-theme/-/tags'
pkgdir=$(readlink -f $(dirname "$0"))
verme=$(cut -d ' ' -f 1 "$pkgdir/version")

mkdir -p "/pkg/tmp/$pkgname/src"
cd "/pkg/tmp/$pkgname/src"

verup=$(curl -sS "$url" | grep 'tags/' | cut -d 'gs/' -f 2 | cut -d '"' -f 1 \
        | grep -Ev 'alpha|beta|rc' | head -n 1)

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

vermex="${verme%.*}"
verupx="${verup%.*}"

url="https://download.gnome.org/sources/adwaita-icon-theme/$verupx"
src="
$url/adwaita-icon-theme-$verup.tar.xz
$url/adwaita-icon-theme-$verup.sha256sum
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	xz -dc "adwaita-icon-theme-$verup.tar.xz" | pax -r

	echo "Scanning ..."
	ls | grep -v "adwaita-icon-theme-$verup.tar.xz$" | clamdscan --fdpass --no-summary -i

	rm -rf "adwaita-icon-theme-$verup"

	sha256sum -c "adwaita-icon-theme-$verup.sha256sum"

	echo "Updating $pkgname repo files"
	sum=$(b3sum "adwaita-icon-theme-$verup.tar.xz")

	sed "s|.*adwaita-icon-theme.*.tar.xz|$sum|" "$pkgdir/checksums" > _
	mv -f _ "$pkgdir/checksums"

	sed "s|$verme|$verup|;s|$vermex|$verupx|" "$pkgdir/sources" > _
	mv -f _ "$pkgdir/sources"

	echo "$verup 1" > "$pkgdir/version"
	echo "$pkgname: $verme -> $verup"
fi
