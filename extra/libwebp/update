#!/bin/sh -e

pkgname=libwebp
pkgdir=$(readlink -f $(dirname $0))
url=https://chromium.googlesource.com/webm/libwebp
verme=$(cut -d ' ' -f 1 $pkgdir/version)
verup=$(curl -sS "$url" | tr 'RefL' '\n' | grep 'tags' | grep -v 'rc' \
        | head -n 1 | cut -d 'v' -f 2 | cut -d '"' -f 1)

mkdir -p /pkg/tmp/$pkgname/src
cd /pkg/tmp/$pkgname/src

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

url=https://storage.googleapis.com/downloads.webmproject.org/releases/webp
src="
$url/libwebp-$verup.tar.gz
$url/libwebp-$verup.tar.gz.asc
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	gzip -dc libwebp-$verup.tar.gz | pax -r

	echo "Scanning ..."
	ls | grep -v "libwebp-$verup.tar.gz$" | clamdscan --fdpass --no-summary -i

	rm -rf libwebp-$verup

	gpg --verify libwebp-$verup.tar.gz.asc libwebp-$verup.tar.gz

	echo "Updating $pkgname repo files"
	sum=$(b3sum libwebp-$verup.tar.gz)

	sed "s|.*libwebp.*.tar.gz|$sum|" $pkgdir/checksums > _
	mv -f _ $pkgdir/checksums

	sed "s/$verme/$verup/g" $pkgdir/sources > _
	mv -f _ $pkgdir/sources

	echo "$verup 1" > $pkgdir/version
	echo "$pkgname: $verme -> $verup"
fi
