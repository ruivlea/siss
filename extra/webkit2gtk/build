#!/bin/sh -e

patch -p1 < cloopfix.patch

export DESTDIR="$1"
export CCACHE_SLOPPINESS=time_macros,include_file_mtime

pkg-config --exists gstreamer-play-1.0 && streaming=ON
pkg-config --exists libopenjp2 && jpeg=ON
pkg-config --exists libtasn1 && tasn=ON
pkg-config --exists libsecret-1 && secret=ON
pkg-config --exists libwoff2common && woff2=ON

# Use clang if installed, decreases compilation time by 25%.
if command -v clang; then
	export CC=clang
	export CXX=clang++
fi

# Fix clang build.
sed -e '/LC_ALL/d' -e '/WTFLogAlways/d' Source/JavaScriptCore/jsc.cpp > _
mv -f _ Source/JavaScriptCore/jsc.cpp

# Remove gettext requirement.
sed 's/ngettext/printf/g' Tools/MiniBrowser/gtk/BrowserDownloadsBar.c > _
mv -f _ Tools/MiniBrowser/gtk/BrowserDownloadsBar.c
sed '/po_files \*\.po/d' Source/WebCore/platform/gtk/po/CMakeLists.txt > _
mv -f _ Source/WebCore/platform/gtk/po/CMakeLists.txt
sed '/if.*GETTEXT_FOUND/,/^endif/d' Source/WebCore/platform/gtk/po/CMakeLists.txt > _
mv -f _ Source/WebCore/platform/gtk/po/CMakeLists.txt
sed '/^GETTEXT_C/d' Source/WebCore/platform/gtk/po/CMakeLists.txt > _
mv -f _ Source/WebCore/platform/gtk/po/CMakeLists.txt

# Webkit's CMake configuration forces color output using clang-specific flags
# when using Ninja as the CMAKE_GENERATOR. We should disable them.
sed -e "s/-fcolor-diagnostics/-fno-color-diagnostics/" \
    -e "s/-fdiagnostics-color=always/-fdiagnostics-color=never/" \
    Source/cmake/WebKitCompilerFlags.cmake > _
mv -f _ Source/cmake/WebKitCompilerFlags.cmake

# Reduce memory usage.
export LDFLAGS="$LDFLAGS -Wl,--no-keep-memory"

# If using mold as system linker, switch since mold cannot link all of webkit (yet).
if ls -l /usr/bin/ld | grep 'mold'; then
	for linker in ld.bfd ld.lld ld.gold; do
		if command -v $linker; then
			ln -s /usr/bin/$linker ld
			break
		fi
	done
	export PATH="$PWD:$PATH"
fi

# sbase ln doesn't have 'h' and 'n' args
sed 's/ln -sfh/ln -sf/g' Source/WebCore/Scripts/generate-derived-sources.sh > _
mv -f _ Source/WebCore/Scripts/generate-derived-sources.sh
chmod +x Source/WebCore/Scripts/generate-derived-sources.sh

for f in \
	Source/JavaScriptCore/GLib.cmake \
	Source/WebKit/PlatformGTK.cmake \
	Source/WebKit/PlatformWPEDeprecated.cmake \
	Source/WebKit/PlatformGTKDeprecated.cmake;
do
	sed 's/ln -n -s -f/ln -sf/g' $f > _
	mv -f _ $f
done

# Out of memory if build with samu with -pipe with 8gb ram
cmake -B build -G Ninja \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBEXECDIR=/usr/lib \
    -DCMAKE_SKIP_RPATH=ON \
    -DLIB_INSTALL_DIR=/usr/lib \
    -DPORT=GTK \
    -DENABLE_BUBBLEWRAP_SANDBOX=OFF \
    -DENABLE_SAMPLING_PROFILER=OFF \
    -DENABLE_GEOLOCATION=OFF \
    -DENABLE_GLES2=ON \
    -DENABLE_INTROSPECTION=OFF \
    -DENABLE_MINIBROWSER=OFF \
    -DENABLE_SPELLCHECK=OFF \
    -DENABLE_WEB_CRYPTO=${tasn:-OFF} \
    -DENABLE_VIDEO=${streaming:-OFF} \
    -DENABLE_WEB_AUDIO=${streaming:-OFF} \
    -DENABLE_GAMEPAD=OFF \
    -DENABLE_JOURNALD_LOG=OFF \
    -DENABLE_DOCUMENTATION=OFF \
    -DENABLE_WAYLAND_TARGET=OFF \
    -DUSE_SOUP2=ON \
    -DUSE_WOFF2=${woff2:-OFF} \
    -DUSE_WPE_RENDERER=OFF \
    -DUSE_LIBSECRET=${secret:-OFF} \
    -DUSE_LIBHYPHEN=OFF \
    -DUSE_OPENJPEG=${jpeg:-OFF} \
    -DUSE_LCMS=OFF \
    -DUSE_AVIF=OFF \
    -DUSE_JPEGXL=OFF \
    -DUSE_GSTREAMER_TRANSCODER=OFF \
    -DUSE_GTK4=OFF \
    -Wno-dev

cmake --build build
cmake --install build
