#!/bin/sh -e

pkgname=gobject-introspection

url='https://gitlab.alpinelinux.org/alpine/aports.git'
stables=$(git ls-remote --heads "$url" | grep 'stable' | cut -d 'heads/' -f 2)
maj=$(echo "$stables" | cut -d '.' -f 1 | sort -ru | head -n 1)
min=$(echo "$stables" | grep "$maj" | cut -d '.' -f 2 | sort -rn | head -n 1)
url="https://git.alpinelinux.org/aports/plain/main/$pkgname/APKBUILD?h=$maj.$min"

pkgdir=$(readlink -f $(dirname "$0"))
verme=$(cut -d ' ' -f 1 "$pkgdir/version")

mkdir -p /pkg/tmp/$pkgname/src
cd /pkg/tmp/$pkgname/src

verup=$(curl -sS "$url" | grep 'pkgver=' | cut -d '=' -f 2)

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

vermex="${verme%.*}"
verupx="${verup%.*}"

url="https://download.gnome.org/sources/gobject-introspection/$verupx"
src="
$url/gobject-introspection-$verup.tar.xz
$url/gobject-introspection-$verup.sha256sum
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	xz -dc "gobject-introspection-$verup.tar.xz" | pax -r

	echo "Scanning ..."
	ls | grep -v "gobject-introspection-$verup.tar.xz$" \
       | clamdscan --fdpass --no-summary -i

	rm -rf "gobject-introspection-$verup"

	echo "Checking sum ..."
	sumup=$(grep '.tar' "gobject-introspection-$verup.sha256sum" \
            | cut -d ' ' -f 1)
	sum=$(sha256sum "gobject-introspection-$verup.tar.xz" | cut -d ' ' -f 1)
	if [ "$sum" != "$sumup" ]; then
		echo "$pkgname: gobject-introspection-$verup.tar.xz sum failed"
		exit 1
	fi

	echo "Updating $pkgname repo files"
	sum=$(b3sum "gobject-introspection-$verup.tar.xz")

	sed "s|.*gobject-introspection.*.tar.xz|$sum|" "$pkgdir/checksums" > _
	mv -f _ "$pkgdir/checksums"

	sed "s/$verme/$verup/;s/$vermex/$verupx/" "$pkgdir/sources" > _
	mv -f _ "$pkgdir/sources"

	echo "$verup 1" > "$pkgdir/version"
	echo "$pkgname: $verme -> $verup"
fi
