#!/bin/sh -e

pkgname=tiff
url=https://libtiff.gitlab.io/libtiff/
pkgdir=$(readlink -f $(dirname $0))
verme=$(cut -d ' ' -f 1 $pkgdir/version)

mkdir -p /pkg/tmp/$pkgname/src
cd /pkg/tmp/$pkgname/src

verup=$(curl -sS "$url" | grep '>v' | head -n 1 | cut -d 'v' -f 2 \
        | cut -d '<' -f 1)

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

url=https://download.osgeo.org/libtiff
src="
$url/tiff-$verup.tar.xz
$url/tiff-$verup.tar.xz.sig
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	xz -dc tiff-$verup.tar.xz | pax -r

	echo "Scanning ..."
	ls | grep -v "tiff-$verup.tar.xz$" | clamdscan --fdpass --no-summary -i

	rm -rf tiff-$verup

	gpg --verify tiff-$verup.tar.xz.sig tiff-$verup.tar.xz

	echo "Updating $pkgname repo files"
	sum=$(b3sum tiff-$verup.tar.xz)

	sed "s|.*tiff.*.tar.xz|$sum|" $pkgdir/checksums > _
	mv -f _ $pkgdir/checksums

	sed "s/$verme/$verup/" $pkgdir/sources > _
	mv -f _ $pkgdir/sources

	echo "$verup 1" > $pkgdir/version
	echo "$pkgname: $verme -> $verup"
fi
