#!/bin/sh -e

pkgname=cmake
url='https://cmake.org/download/'
pkgdir=$(readlink -f $(dirname "$0"))
verme=$(cut -d ' ' -f 1 "$pkgdir/version")

mkdir -p "/pkg/tmp/$pkgname/src"
cd "/pkg/tmp/$pkgname/src"

verup=$(curl -sS "$url" | grep 'Latest' | head -n 1 | cut -d '(' -f 2 \
        | cut -d ')' -f 1)

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

vermex="${verme%.*}"
verupx="${verup%.*}"

url="${url%/d*}/files/v$verupx"
src="
$url/cmake-$verup.tar.gz
$url/cmake-$verup-SHA-256.txt
$url/cmake-$verup-SHA-256.txt.asc
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	gzip -dc "cmake-$verup.tar.gz" | pax -r

	echo "Scanning ..."
	ls | grep -v "cmake-$verup.tar.gz$" | clamdscan --fdpass --no-summary -i

	rm -rf "cmake-$verup"

	gpg --verify "cmake-$verup-SHA-256.txt.asc" "cmake-$verup-SHA-256.txt"

	sumup=$(grep "$verup.tar.gz$" "cmake-$verup-SHA-256.txt" | cut -d ' ' -f 1)
	sum=$(sha256sum "cmake-$verup.tar.gz" | cut -d ' ' -f 1)
	if [ "$sum" != "$sumup" ]; then
		echo "$pkgname: cmake-$verup.tar.gz sum failed"
		exit 1
	fi

	echo "Updating $pkgname repo files"
	sum=$(b3sum "cmake-$verup.tar.gz")

	sed "s|.*cmake.*.tar.gz|$sum|" "$pkgdir/checksums" > _
	mv -f _ "$pkgdir/checksums"

	sed "s/$verme/$verup/;s/$vermex/$verupx/" "$pkgdir/sources" > _
	mv -f _ "$pkgdir/sources"

	echo "$verup 1" > "$pkgdir/version"
	echo "$pkgname: $verme -> $verup"
fi
