#!/bin/sh -e

pkgname=unifont
url="https://unifoundry.com/unifont/index.html"
pkgdir=$(readlink -f $(dirname "$0"))
verme=$(cut -d ' ' -f 1 "$pkgdir/version")

mkdir -p "/pkg/tmp/$pkgname/src"
cd "/pkg/tmp/$pkgname/src"

verup=$(curl -sS "$url" | grep '(Un' | head -n 1 | cut -d 't ' -f 2 \
        | cut -d ')' -f 1)

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

url="${url%/u*}/pub/unifont/unifont-$verup"
src="
$url/unifont-$verup.tar.gz
$url/unifont-$verup.tar.gz.sig
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	gzip -dc "unifont-$verup.tar.gz" | pax -r

	echo "Scanning ..."
	ls | grep -v "unifont-$verup.tar.gz$" | clamdscan --fdpass --no-summary -i

	rm -rf "unifont-$verup"

	gpg --verify "unifont-$verup.tar.gz.sig" "unifont-$verup.tar.gz"

	echo "Updating $pkgname repo files"
	sum=$(b3sum "unifont-$verup.tar.gz")

	sed "s|.*unifont.*.tar.gz|$sum|" "$pkgdir/checksums" > _
	mv -f _ "$pkgdir/checksums"

	sed "s/$verme/$verup/g" "$pkgdir/sources" > _
	mv -f _ "$pkgdir/sources"

	echo "$verup 1" > "$pkgdir/version"
	echo "$pkgname: $verme -> $verup"
fi
