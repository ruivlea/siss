#!/bin/sh -e

pkgname=mingw-w64-binutils
url='https://www.gnu.org/software/binutils/'
pkgdir=$(readlink -f $(dirname "$0"))
verme=$(cut -d ' ' -f 1 "$pkgdir/version")

mkdir -p "/pkg/tmp/$pkgname/src"
cd "/pkg/tmp/$pkgname/src"

verup=$(curl -sS "$url" | grep 'latest' | head -n 1 | cut -d 'is ' -f 2 \
        | cut -d '. ' -f 1)

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

url='https://ftp.gnu.org/gnu/binutils'
src="
$url/binutils-$verup.tar.xz
$url/binutils-$verup.tar.xz.sig
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	xz -dc "binutils-$verup.tar.xz" | pax -r

	echo "Scanning ..."
	ls | grep -v "binutils-$verup.tar.xz$" | clamdscan --fdpass --no-summary -i

	rm -rf "binutils-$verup"

	gpg --verify "binutils-$verup.tar.xz.sig" "binutils-$verup.tar.xz"

	echo "Updating $pkgname repo files"
	sum=$(b3sum "binutils-$verup.tar.xz")

	sed "s|.*binutils.*.tar.xz|$sum|" "$pkgdir/checksums" > _
	mv -f _ "$pkgdir/checksums"

	sed "s/$verme/$verup/" "$pkgdir/sources" > _
	mv -f _ "$pkgdir/sources"

	echo "$verup 1" > "$pkgdir/version"
	echo "$pkgname: $verme -> $verup"
fi
