#!/bin/sh -e

pkgname=gst-plugins-good
pkgdir=$(readlink -f $(dirname "$0"))
verme=$(cut -d ' ' -f 1 "$pkgdir/version")

url='https://gitlab.alpinelinux.org/alpine/aports.git'
stables=$(git ls-remote --heads "$url" | grep 'stable' | cut -d 'heads/' -f 2)
maj=$(echo "$stables" | cut -d '.' -f 1 | sort -ru | head -n 1)
min=$(echo "$stables" | grep "$maj" | cut -d '.' -f 2 | sort -rn | head -n 1)
url="https://git.alpinelinux.org/aports/plain"
url="$url/community/$pkgname/APKBUILD?h=$maj.$min"

verup=$(curl -sS "$url" | grep 'pkgver=' | cut -d '=' -f 2)

mkdir -p "/pkg/tmp/$pkgname/src"
cd "/pkg/tmp/$pkgname/src"

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

url='https://gstreamer.freedesktop.org/src/gst-plugins-good'
src="
$url/gst-plugins-good-$verup.tar.xz
$url/gst-plugins-good-$verup.tar.xz.asc
$url/gst-plugins-good-$verup.tar.xz.sha256sum
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	xz -dc "gst-plugins-good-$verup.tar.xz" | pax -r

	echo "Scanning ..."
	ls | grep -v "gst-plugins-good-$verup.tar.xz$" \
       | clamdscan --fdpass --no-summary -i

	rm -rf "gst-plugins-good-$verup"

	gpg --verify "gst-plugins-good-$verup.tar.xz.asc"
	sha256sum -c "gst-plugins-good-$verup.tar.xz.sha256sum"

	echo "Updating $pkgname repo files"
	sum=$(b3sum "gst-plugins-good-$verup.tar.xz")

	sed "s|.*gst-plugins-good.*.tar.xz|$sum|" "$pkgdir/checksums" > _
	mv -f _ "$pkgdir/checksums"

	sed "s/$verme/$verup/" "$pkgdir/sources" > _
	mv -f _ "$pkgdir/sources"

	echo "$verup 1" > "$pkgdir/version"
	echo "$pkgname: $verme -> $verup"
fi
