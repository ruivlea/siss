#!/bin/sh -e

pkgname=saait
url='https://codemadness.org/releases/saait/'
pkgdir=$(readlink -f $(dirname "$0"))
verme=$(cut -d ' ' -f 1 "$pkgdir/version")

mkdir -p "/pkg/tmp/$pkgname/src"
cd "/pkg/tmp/$pkgname/src"

verup=$(curl -sS "$url" | grep 'gz' | tail -n 1 | cut -d '-' -f 2 \
        | cut -d '.t' -f 1)

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

src="
${url}saait-$verup.tar.gz
${url}saait-$verup.tar.gz.sha256
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	gzip -dc "saait-$verup.tar.gz" | pax -r

	echo "Scanning ..."
	ls | grep -v "saait-$verup.tar.gz$" | clamdscan --fdpass --no-summary -i

	rm -rf "saait-$verup"

	echo "Checking sum ..."
	sumup=$(cut -d '= ' -f 2 "saait-$verup.tar.gz.sha256")
	sum=$(sha256sum "saait-$verup.tar.gz" | cut -d ' ' -f 1)
	if [ "$sum" != "$sumup" ]; then
		echo "$pkgname: saait-$verup.tar.gz sum failed"
		exit 1
	fi

	echo "Updating $pkgname repo files"
	sum=$(b3sum "saait-$verup.tar.gz")

	sed "s|.*saait.*.tar.gz|$sum|" "$pkgdir/checksums" > _
	mv -f _ "$pkgdir/checksums"

	sed "s/$verme/$verup/g" "$pkgdir/sources" > _
	mv -f _ "$pkgdir/sources"

	echo "$verup 1" > "$pkgdir/version"
	echo "$pkgname: $verme -> $verup"
fi
