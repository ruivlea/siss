#!/bin/sh -e

pkgname=python-installer
url='https://pypi.org/project/installer/'
pkgdir=$(readlink -f $(dirname "$0"))
verme=$(cut -d ' ' -f 1 "$pkgdir/version")

mkdir -p "/pkg/tmp/$pkgname/src"
cd "/pkg/tmp/$pkgname/src"

echo "Downloading $url for version checking"
curl -f "$url" -o _

verup=$(grep 'installer-' _ | sed -n 1p | cut -d '-' -f 2 | cut -d '.t' -f 1)
line=$(grep -n "Hashes for installer-$verup-py3-none-any.whl" _ \
       | cut -d ':' -f 1)
line=$((line + 11))
sumup=$(sed "${line}q;d" _ | cut -d '">' -f 2 | cut -d '<' -f 1)

rm -f _

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

src="https://files.pythonhosted.org/packages/py3/i/installer/installer-$verup-py3-none-any.whl"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	echo "Downloading $src"
	curl -fLO "$src"

	echo "Unpacking ..."
	unzip -d "installer-$verup" "installer-$verup-py3-none-any.whl"

	echo "Scanning ..."
	ls | grep -v "installer-$verup-py3-none-any.whl$" \
       | clamdscan --fdpass --no-summary -i

	rm -rf "installer-$verup"

	echo "Checking sum ..."
	sum=$(sha256sum "installer-$verup-py3-none-any.whl" | cut -d ' ' -f 1)
	if [ "$sum" != "$sumup" ]; then
		echo "$pkgname: installer-$verup-py3-none-any.whl sum failed"
		exit 1
	fi

	echo "Updating $pkgname repo files"
	sum=$(b3sum "installer-$verup-py3-none-any.whl")

	sed "s|.*installer.*.whl|$sum|" "$pkgdir/checksums" > _
	mv -f _ "$pkgdir/checksums"

	sed "s/$verme/$verup/" "$pkgdir/sources" > _
	mv -f _ "$pkgdir/sources"

	echo "$verup 1" > "$pkgdir/version"
	echo "$pkgname: $verme -> $verup"
fi
