#!/bin/sh -e

export DESTDIR="$1"

# Some packages require that gtk+3 have atk methods.
# So if atk is installed, then we don't patch it out of gtk, just fribidi.
if pkg-config --exists atk; then
	patch -p1 < 01-no-fribidi.patch
else
	for p in *.patch; do
		patch -p1 < $p
	done
fi

# Disable native language support, disable atk-bridge, don't compile schemas.
sed -e '/compile_schemas/s/true/false/' \
    -e '/ENABLE_NLS/s/1/0/' \
    -e "/subdir('po/d" \
    -e "/atk-bridge/d" \
    meson.build > _
mv -f _ meson.build

# Remove atk-bridge code.
sed '/<atk-bridge.h>/d;/atk_bridge_adaptor_init/d' \
    gtk/a11y/gtkaccessibility.c > _
mv -f _ gtk/a11y/gtkaccessibility.c

# gimp needs gobject-introspection
pkg-config --exists gobject-introspection-1.0 && gi=true

meson setup \
    -Dprefix=/usr \
    -Db_colorout=never \
    -Dx11_backend=true \
    -Dwayland_backend=false \
    -Dxinerama=yes \
    -Dprint_backends=file,lpr \
    -Dcolord=no \
    -Dintrospection="${gi:-false}" \
    -Ddemos=false \
    -Dexamples=false \
    -Dtests=false \
    output

ninja -C output
ninja -C output install

# If no librsvg, delete it because this utility causing
# compiler errors for some packages.
pkg-config --exist librsvg-2.0 || rm -f "$1/usr/bin/gtk-encode-symbolic-svg"
