#!/bin/sh -e

pkgname=nss
url=https://ftp.mozilla.org/pub/security/nss/releases/
pkgdir=$(readlink -f $(dirname $0))
verme=$(cut -d ' ' -f 1 $pkgdir/version)
vermex=$(echo $verme | tr '.' '_')

mkdir -p /pkg/tmp/$pkgname/src
cd /pkg/tmp/$pkgname/src

echo "Downloading $url for version checking"
curl -f "$url" -o _

verupx=$(grep 'NSS_' _ | cut -d '_' -f 2 | sort -rn | head -n 1)
verupx=${verupx}_$(grep "S_$verupx" _ | cut -d '_' -f 3 | sort -rn \
                   | head -n 1)
verupx=${verupx}_$(grep "S_$verupx" _ | cut -d '_R' -f 1 | cut -d '_' -f 4 \
                   | sort -rn | head -n 1)
verupx=${verupx%_}
verup=$(echo $verupx | tr '_' '.')

rm -f _

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

url=${url}NSS_${verupx}_RTM/src
src="
$url/nss-$verup.tar.gz
$url/SHA256SUMS
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	gzip -dc nss-$verup.tar.gz | pax -r

	echo "Scanning ..."
	ls | grep -v "nss-$verup.tar.gz$" | clamdscan --fdpass --no-summary -i

	rm -rf nss-$verup

	echo "Checking sum ..."
	sumup=$(grep "nss-$verup.tar.gz$" SHA256SUMS | cut -d ' ' -f 1)
	sum=$(sha256sum nss-$verup.tar.gz | cut -d ' ' -f 1)
	if [ "$sum" != "$sumup" ]; then
		echo "$pkgname: nss-$verup.tar.gz sum failed"
		exit 1
	fi

	echo "Updating $pkgname repo files"
	sum=$(b3sum nss-$verup.tar.gz)

	sed "s|.*nss.*.tar.gz|$sum|" $pkgdir/checksums > _
	mv -f _ $pkgdir/checksums

	vermey=$(echo "$verme" | sed 's/\./\\./g')
	sed "s/$vermex/$verupx/;s/$vermey/$verup/" $pkgdir/sources > _
	mv -f _ $pkgdir/sources

	echo "$verup 1" > $pkgdir/version
	echo "$pkgname: $verme -> $verup"
fi
