#!/bin/sh -e

# Stripping binaries breaks go.
:> nostrip

patch -p1 < no-bash.patch
rm -f src/make.bash.orig

if [ ! -d /pkg/installed/go ] && [ ! -d /pkg/installed/go-bootstrap ]; then
	echo "Please install go-bootstrap"
	exit 1
fi

export CC=cc
export GOARCH=amd64
export GO_LDFLAGS="-w -s"

export GOROOT_BOOTSTRAP=/usr/lib/go1.17.13
export GOROOT=$PWD
export GOROOT_FINAL=/usr/lib/go

(
	cd src
	./make.bash -v
)

cd $GOROOT

install -Dm 755 bin/go $1/usr/lib/go/bin/go
install -Dm 755 bin/gofmt $1/usr/lib/go/bin/gofmt

mkdir -p $1/usr/bin
ln -s /usr/lib/go/bin/go $1/usr/bin/go
ln -s /usr/lib/go/bin/gofmt $1/usr/bin/gofmt

cp -pPR misc pkg src lib go.env $1/usr/lib/go

# Remove unneeded files.
rm -f $1/usr/share/go/doc/articles/wiki/get.bin
rm -f $1/usr/lib/go/pkg/tool/*/api
rm -rf $1/usr/lib/go/pkg/bootstrap
rm -rf $1/usr/lib/go/pkg/obj

# Remove tests.
cd $1/usr/lib/go/src
find . -type f -a -name \*_test.go -depth -exec rm {} +
find . -type f -a -name \*.bash -depth -exec rm {} +
find . -type f -a -name \*.bat -depth -exec rm {} +
find . -type f -a -name \*.rc -depth -exec rm {} +

find . -type d -a -name testdata | while read -r dir; do
	rm -rf "$dir"
done
