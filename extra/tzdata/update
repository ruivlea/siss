#!/bin/sh -e

pkgname=tzdata
url=https://www.iana.org/time-zones
pkgdir=$(readlink -f $(dirname $0))
verme=$(cut -d ' ' -f 1 $pkgdir/version)

mkdir -p /pkg/tmp/$pkgname/src
cd /pkg/tmp/$pkgname/src

verup=$(curl -sS "$url" | grep 'Released' | head -n 1 | cut -d '>' -f 2 \
        | cut -d '<' -f 1)

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

url=https://data.iana.org/time-zones/releases
src="
$url/tzcode$verup.tar.gz
$url/tzcode$verup.tar.gz.asc
$url/tzdata$verup.tar.gz
$url/tzdata$verup.tar.gz.asc
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	mkdir tmp
	cd tmp
	gzip -dc ../tzcode$verup.tar.gz | pax -r
	gzip -dc ../tzdata$verup.tar.gz | pax -r
	cd ..

	echo "Scanning ..."
	ls | grep -v "tar.gz$" | clamdscan --fdpass --no-summary -i

	rm -rf tmp

	gpg --verify tzcode$verup.tar.gz.asc tzcode$verup.tar.gz
	gpg --verify tzdata$verup.tar.gz.asc tzdata$verup.tar.gz

	echo "Updating $pkgname repo files"
	sum1=$(b3sum tzcode$verup.tar.gz)
	sum2=$(b3sum tzdata$verup.tar.gz)

	sed -e "s|.*tzcode.*.tar.gz|$sum1|" \
        -e "s|.*tzdata.*.tar.gz|$sum2|" \
        $pkgdir/checksums > _
	mv -f _ $pkgdir/checksums

	sed "s/$verme/$verup/g" $pkgdir/sources > _
	mv -f _ $pkgdir/sources

	echo "$verup 1" > $pkgdir/version
	echo "$pkgname: $verme -> $verup"
fi
