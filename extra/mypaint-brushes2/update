#!/bin/sh -e

pkgname=mypaint-brushes2
url=https://github.com/mypaint/mypaint-brushes/releases
pkgdir=$(readlink -f $(dirname $0))
verme=$(cut -d ' ' -f 1 $pkgdir/version)

mkdir -p /pkg/tmp/$pkgname/src
cd /pkg/tmp/$pkgname/src

echo "Downloading $url for version checking"
curl -f "$url" -o _

verup=$(grep 'tag/' _ | grep -v 'pre' | cut -d '/v' -f 2 | cut -d '.' -f 1 \
        | sort -rn | head -n 1)
verup=$verup.$(grep 'tag/' _ | grep -v 'pre' | cut -d '/v' -f 2 \
               | cut -d '"' -f 1 | grep "^$verup" | cut -d '.' -f 2 \
               | sort -rn | head -n 1)
verup=$verup.$(grep 'tag/' _ | grep -v 'pre' | cut -d '/v' -f 2 \
               | cut -d '"' -f 1 | grep "^$verup" | cut -d '.' -f 3 \
               | sort -rn | head -n 1)

rm -f _

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

url=$url/download/v$verup
src="
$url/mypaint-brushes-$verup.tar.xz
$url/mypaint-brushes-$verup.sha256.asc
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	xz -dc mypaint-brushes-$verup.tar.xz | pax -r

	echo "Scanning ..."
	ls | grep -v "mypaint-brushes-$verup.tar.xz$" | clamdscan --fdpass --no-summary -i

	rm -rf mypaint-brushes-$verup

	gpg --verify mypaint-brushes-$verup.sha256.asc

	echo "Checking sum ..."
	sumup=$(grep '.tar' mypaint-brushes-$verup.sha256.asc | cut -d ' ' -f 1)
	sum=$(sha256sum mypaint-brushes-$verup.tar.xz | cut -d ' ' -f 1)
	if [ "$sum" != "$sumup" ]; then
		echo "$pkgname: mypaint-brushes-$verup.tar.xz sum failed"
		exit 1
	fi

	echo "Updating $pkgname repo files"
	sum=$(b3sum mypaint-brushes-$verup.tar.xz)

	sed "s|.*mypaint-brushes.*.tar.xz|$sum|" $pkgdir/checksums > _
	mv -f _ $pkgdir/checksums

	sed "s/$verme/$verup/g" $pkgdir/sources > _
	mv -f _ $pkgdir/sources

	echo "$verup 1" > $pkgdir/version
	echo "$pkgname: $verme -> $verup"
fi
