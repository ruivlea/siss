#!/bin/sh -e

pkgname=dosfstools
url='https://github.com/dosfstools/dosfstools/releases'
pkgdir=$(readlink -f $(dirname "$0"))
verme=$(cut -d ' ' -f 1 "$pkgdir/version")

mkdir -p "/pkg/tmp/$pkgname/src"
cd "/pkg/tmp/$pkgname/src"

verup=$(curl -sS "$url" | grep 'tag/' | head -n 1 | cut -d '/v' -f 2 \
        | cut -d '"' -f 1)

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

url="$url/download/v$verup"
src="
$url/dosfstools-$verup.tar.gz
$url/dosfstools-$verup.tar.gz.sig
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	gzip -dc "dosfstools-$verup.tar.gz" | pax -r

	echo "Scanning ..."
	ls | grep -v "dosfstools-$verup.tar.gz$" | clamdscan --fdpass --no-summary -i

	rm -rf "dosfstools-$verup"

	gpg --verify "dosfstools-$verup.tar.gz.sig" "dosfstools-$verup.tar.gz"

	echo "Updating $pkgname repo files"
	sum=$(b3sum "dosfstools-$verup.tar.gz")

	sed "s|.*dosfstools.*.tar.gz|$sum|" "$pkgdir/checksums" > _
	mv -f _ "$pkgdir/checksums"

	sed "s/$verme/$verup/g" "$pkgdir/sources" > _
	mv -f _ "$pkgdir/sources"

	echo "$verup 1" > "$pkgdir/version"
	echo "$pkgname: $verme -> $verup"
fi
