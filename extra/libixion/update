#!/bin/sh -e

pkgname=libixion
url=https://gitlab.com/ixion/ixion/-/tags
pkgdir=$(readlink -f $(dirname $0))
verme=$(cut -d ' ' -f 1 $pkgdir/version)

mkdir -p /pkg/tmp/$pkgname/src
cd /pkg/tmp/$pkgname/src

verup=$(curl -sS "$url" | grep 'tags/' | cut -d 's/' -f 2 | cut -d '"' -f 1 \
        | grep -Ev '[^0-9.]' | sed -n 1p)

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

src=https://kohei.us/files/ixion/src/libixion-$verup.tar.xz

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	xz -dc libixion-$verup.tar.xz | pax -r

	echo "Scanning ..."
	ls | grep -v "libixion-$verup.tar.xz$" | clamdscan --fdpass --no-summary -i

	rm -rf libixion-$verup

	echo "Checking sum ..."
	# Listed here but curl can't get because of javascript:
	# https://gitlab.com/ixion/ixion/-/releases
	url=https://gitlab.archlinux.org/archlinux/packaging/packages/libixion/-/raw/main/PKGBUILD
	sumup=$(curl -sS "$url" | grep 'sha256sums=' | cut -d "'" -f 2 \
            | cut -d "'" -f 1)
	sum=$(sha256sum libixion-$verup.tar.xz | cut -d ' ' -f 1)
	if [ "$sum" != "$sumup" ]; then
		echo "$pkgname: libixion-$verup.tar.xz sum failed"
		exit 1
	fi

	echo "Updating $pkgname repo files"
	sum=$(b3sum libixion-$verup.tar.xz)

	sed "s|.*libixion.*.tar.xz|$sum|" $pkgdir/checksums > _
	mv -f _ $pkgdir/checksums

	sed "s/$verme/$verup/" $pkgdir/sources > _
	mv -f _ $pkgdir/sources

	echo "$verup 1" > $pkgdir/version
	echo "$pkgname: $verme -> $verup"
fi
