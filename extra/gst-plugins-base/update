#!/bin/sh -e

pkgname=gst-plugins-base
pkgdir=$(readlink -f $(dirname "$0"))
verme=$(cut -d ' ' -f 1 "$pkgdir/version")

url='https://gitlab.alpinelinux.org/alpine/aports.git'
stables=$(git ls-remote --heads "$url" | grep 'stable' | cut -d 'heads/' -f 2)
maj=$(echo "$stables" | cut -d '.' -f 1 | sort -ru | head -n 1)
min=$(echo "$stables" | grep "$maj" | cut -d '.' -f 2 | sort -rn | head -n 1)
url="https://git.alpinelinux.org/aports/plain"
url="$url/main/$pkgname/APKBUILD?h=$maj.$min"

verup=$(curl -sS "$url" | grep 'pkgver=' | cut -d '=' -f 2)

mkdir -p "/pkg/tmp/$pkgname/src"
cd "/pkg/tmp/$pkgname/src"

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

url='https://gstreamer.freedesktop.org/src/gst-plugins-base'
src="
$url/gst-plugins-base-$verup.tar.xz
$url/gst-plugins-base-$verup.tar.xz.asc
$url/gst-plugins-base-$verup.tar.xz.sha256sum
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	git clone --depth 1 \
        https://gitlab.freedesktop.org/gstreamer/meson-ports/gl-headers.git \
        subprojects/gl-headers

	echo "Unpacking ..."
	xz -dc "gst-plugins-base-$verup.tar.xz" | pax -r

	echo "Scanning ..."
	ls | grep -v "gst-plugins-base-$verup.tar.xz$" \
       | clamdscan --fdpass --no-summary -i

	rm -rf "gst-plugins-base-$verup"

	gpg --verify "gst-plugins-base-$verup.tar.xz.asc"
	sha256sum -c "gst-plugins-base-$verup.tar.xz.sha256sum"

	echo "Updating $pkgname repo files"
	sum=$(b3sum "gst-plugins-base-$verup.tar.xz")

	sed "s|.*gst-plugins-base.*.tar.xz|$sum|" "$pkgdir/checksums" > _
	mv -f _ "$pkgdir/checksums"

	sed "s/$verme/$verup/" "$pkgdir/sources" > _
	mv -f _ "$pkgdir/sources"

	echo "$verup 1" > "$pkgdir/version"
	echo "$pkgname: $verme -> $verup"
fi
