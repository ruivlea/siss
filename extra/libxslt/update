#!/bin/sh -e

pkgname=libxslt
url=https://download.gnome.org/sources/libxslt/
pkgdir=$(readlink -f $(dirname $0))
verme=$(cut -d ' ' -f 1 $pkgdir/version)
vermex=${verme%.*}

mkdir -p /pkg/tmp/$pkgname/src
cd /pkg/tmp/$pkgname/src

echo "Downloading $url for version checking"
curl -f "$url" -o _

verupx=$(grep 'title=' _ | cut -d 'title="' -f 2 | cut -d '"' -f 1 \
         | cut -d '.' -f 1 | sed '/[^0-9]/d' | sort -rn | head -n 1)
verupx=$verupx.$(grep 'title=' _ | cut -d 'title="' -f 2 | cut -d '"' -f 1 \
                 | grep "^$verupx" | cut -d '.' -f 2 | sort -rn | head -n 1)

rm -f _

url=$url$verupx/
verup=$(curl -sS "$url" | grep 'LATEST' | head -n 1 | cut -d 'IS-' -f 2 \
        | cut -d '"' -f 1)

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

src="
${url}libxslt-$verup.tar.xz
${url}libxslt-$verup.sha256sum
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	xz -dc libxslt-$verup.tar.xz | pax -r

	echo "Scanning ..."
	ls | grep -v "libxslt-$verup.tar.xz$" | clamdscan --fdpass --no-summary -i

	rm -rf libxslt-$verup

	echo "Checking sum ..."
	sumup=$(grep '.tar' libxslt-$verup.sha256sum | cut -d ' ' -f 1)
	sum=$(sha256sum libxslt-$verup.tar.xz | cut -d ' ' -f 1)
	if [ "$sum" != "$sumup" ]; then
		echo "$pkgname: libxslt-$verup.tar.xz sum failed"
		exit 1
	fi

	echo "Updating $pkgname repo files"
	sum=$(b3sum libxslt-$verup.tar.xz)

	sed "s|.*libxslt.*.tar.xz|$sum|" $pkgdir/checksums > _
	mv -f _ $pkgdir/checksums

	sed "s/$verme/$verup/;s/$vermex/$verupx/" $pkgdir/sources > _
	mv -f _ $pkgdir/sources

	echo "$verup 1" > $pkgdir/version
	echo "$pkgname: $verme -> $verup"
fi
