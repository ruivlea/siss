#!/bin/sh -e

pkgname=atkmm
pkgdir=$(readlink -f $(dirname "$0"))
verme=$(cut -d ' ' -f 1 "$pkgdir/version")

url='https://git.alpinelinux.org/aports/plain/community/atkmm/APKBUILD'
verup=$(curl -sS "$url" | grep 'pkgver=' | cut -d '=' -f 2)

mkdir -p "/pkg/tmp/$pkgname/src"
cd "/pkg/tmp/$pkgname/src"

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

vermex="${verme%.*}"
verupx="${verup%.*}"

url="https://download.gnome.org/sources/atkmm/$verupx"
src="
$url/atkmm-$verup.tar.xz
$url/atkmm-$verup.sha256sum
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	xz -dc "atkmm-$verup.tar.xz" | pax -r

	echo "Scanning ..."
	ls | grep -v "atkmm-$verup.tar.xz$" | clamdscan --fdpass --no-summary -i

	rm -rf "atkmm-$verup"

	echo "Checking sum ..."
	sumup=$(grep '.tar' "atkmm-$verup.sha256sum" | cut -d ' ' -f 1)
	sum=$(sha256sum "atkmm-$verup.tar.xz" | cut -d ' ' -f 1)
	if [ "$sum" != "$sumup" ]; then
		echo "$pkgname: atkmm-$verup.tar.xz sum failed"
		exit 1
	fi

	echo "Updating $pkgname repo files"
	sum=$(b3sum "atkmm-$verup.tar.xz")

	sed "s|.*atkmm.*.tar.xz|$sum|" "$pkgdir/checksums" > _
	mv -f _ "$pkgdir/checksums"

	sed "s/$verme/$verup/;s/$vermex/$verupx/" "$pkgdir/sources" > _
	mv -f _ "$pkgdir/sources"

	echo "$verup 1" > "$pkgdir/version"
	echo "$pkgname: $verme -> $verup"
fi
