#!/bin/sh -e

pkgname=signify
url='https://github.com/aperezdc/signify/releases'
pkgdir=$(readlink -f $(dirname "$0"))
verme=$(cut -d ' ' -f 1 "$pkgdir/version")

mkdir -p "/pkg/tmp/$pkgname/src"
cd "/pkg/tmp/$pkgname/src"

verup=$(curl -sS "$url" | grep 'tag/' | sed -n 1p | cut -d 'g/' -f 2 \
        | cut -d '"' -f 1 | sed 's/v//')

if [ -z "$verup" ]; then
	echo "$pkgname: verup failed"
	exit 1
fi

url="$url/download/v$verup"
src="
$url/signify-$verup.tar.xz
$url/signify-$verup.tar.xz.asc
$url/SHA256.sig
$url/signifyportable.pub.asc
https://raw.githubusercontent.com/aperezdc/signify/master/keys/signifyportable.pub
"

if [ "$verme" != "$verup" ]; then
	rm -rf *

	for s in $src; do
		echo "Downloading $s"
		curl -fLO "$s"
	done

	echo "Unpacking ..."
	xz -dc "signify-$verup.tar.xz" | pax -r

	echo "Scanning ..."
	ls | grep -v "signify-$verup.tar.xz$" | clamdscan --fdpass --no-summary -i

	rm -rf "signify-$verup"

	fingerprint1="$(gpg --verify "signify-$verup.tar.xz.asc" \
                    "signify-$verup.tar.xz" 2>&1)"
	fingerprint1="$(echo "$fingerprint1" | grep 'fingerprint' \
                    | cut -d ': ' -f 2)"

	fingerprint2="$(gpg --yes --verify -o signifyportable.pub \
                    signifyportable.pub.asc 2>&1)"
	fingerprint2="$(echo "$fingerprint2" | grep 'fingerprint' \
                    | cut -d ': ' -f 2)"

	if [ "$fingerprint1" != "$fingerprint2" ]; then
		echo "$pkgname: fingerprint not match"
		exit 1
	fi

	signify -C -p signifyportable.pub -x SHA256.sig

	echo "Updating $pkgname repo files"
	sum=$(b3sum "signify-$verup.tar.xz")

	sed "s|.*signify.*.tar.xz|$sum|" "$pkgdir/checksums" > _
	mv -f _ "$pkgdir/checksums"

	sed "s/$verme/$verup/g" "$pkgdir/sources" > _
	mv -f _ "$pkgdir/sources"

	echo "$verup 1" > "$pkgdir/version"
	echo "$pkgname: $verme -> $verup"
fi
